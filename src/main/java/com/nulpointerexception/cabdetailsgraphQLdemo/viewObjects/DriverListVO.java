package com.nulpointerexception.cabdetailsgraphQLdemo.viewObjects;

import lombok.*;
import org.springframework.hateoas.RepresentationModel;

import java.io.Serializable;
import java.util.List;

@Getter @Setter @ToString
@RequiredArgsConstructor
public class DriverListVO extends RepresentationModel implements Serializable {
    private static final long serialVersionUID = 1L;
    @NonNull
    private List<DriverVO> driverVOList;
}
