package com.nulpointerexception.cabdetailsgraphQLdemo.viewObjects;

import com.nulpointerexception.cabdetailsgraphQLdemo.entities.Car;
import com.nulpointerexception.cabdetailsgraphQLdemo.entities.Driver;
import lombok.*;
import org.springframework.hateoas.RepresentationModel;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class CarVO extends RepresentationModel implements Serializable {
    private static final long serialVersionUID = 1L;
    @NonNull
    private Car car;
    private String id;
    private String carNumber;
    private DriverVO driverVO;

    public CarVO(Car car){
        this.id = car.getId();
        this.carNumber = car.getCarNumber();
        this.driverVO = new DriverVO(car.getDriverId());
    }


}
