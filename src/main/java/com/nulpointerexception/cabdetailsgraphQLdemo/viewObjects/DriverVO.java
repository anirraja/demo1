package com.nulpointerexception.cabdetailsgraphQLdemo.viewObjects;

import com.nulpointerexception.cabdetailsgraphQLdemo.entities.Driver;
import lombok.*;
import org.springframework.hateoas.RepresentationModel;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class DriverVO extends RepresentationModel implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private String firstname;
    private String lastname;

    public DriverVO(Driver driver){
        this.id = driver.getId();
        this.firstname = driver.getFirstname();
        this.lastname = driver.getLastname();
    }
}
