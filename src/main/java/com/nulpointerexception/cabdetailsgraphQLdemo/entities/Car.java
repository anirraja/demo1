package com.nulpointerexception.cabdetailsgraphQLdemo.entities;

import javax.persistence.*;

@Entity
@Table(name = "cars", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"id"})
})
public class Car {
    @Id
    @Column(name = "id", nullable = false)
    private String id;


    @Column(name = "car_number", nullable = false)
    private String carNumber;

    @ManyToOne
    @JoinColumn(name = "driver_id")
    private Driver driverId;

    public Driver getDriverId() {
        return driverId;
    }

    public void setDriverId(Driver driverId) {
        this.driverId = driverId;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id='" + id + '\'' +
                ", carNumber='" + carNumber + '\'' +
                ", driverId=" + driverId +
                '}';
    }
}