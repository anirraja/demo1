package com.nulpointerexception.cabdetailsgraphQLdemo.graphql;

import com.google.common.io.Resources;
import com.nulpointerexception.cabdetailsgraphQLdemo.graphql.GraphQLDataFetchers;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.*;
import graphql.schema.idl.errors.SchemaProblem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;

@Component
public class GraphQLProvider {
    public static final String SCHEMA_DEFINITION = "schema.graphqls";
    private GraphQL graphQL;

    @Bean
    public GraphQL graphQL(){
        return this.graphQL;
    }

    @Autowired
    GraphQLDataFetchers graphQLDataFetchers;

    @PostConstruct
    public void init() throws IOException, SchemaProblem {
        URL url = Resources.getResource(SCHEMA_DEFINITION);
        String sdl = Resources.toString(url, Charset.forName("UTF-8"));
        GraphQLSchema graphQLSchema = buildSchema(sdl);
        this.graphQL = GraphQL.newGraphQL(graphQLSchema).build();
    }

    private GraphQLSchema buildSchema(String sdl) throws SchemaProblem {
        TypeDefinitionRegistry typeDefinitionRegistry = new SchemaParser().parse(sdl);
        RuntimeWiring runtimeWiring = buildWiring();
        SchemaGenerator schemaGenerator = new SchemaGenerator();
        GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);

        return graphQLSchema;
    }

    private RuntimeWiring buildWiring() {
        return RuntimeWiring.newRuntimeWiring()
                .type(TypeRuntimeWiring.newTypeWiring("Query")
                        .dataFetcher("carById", graphQLDataFetchers.getCabByIdDateFetcher()))
                .type(TypeRuntimeWiring.newTypeWiring("Car")
                        .dataFetcher("driver", graphQLDataFetchers.getDriverDataFetcher()))
                .type(TypeRuntimeWiring.newTypeWiring("Mutation")
                        .dataFetcher("createDriver", graphQLDataFetchers.setDriverDataFetcher()))
                .build();
    }
}
