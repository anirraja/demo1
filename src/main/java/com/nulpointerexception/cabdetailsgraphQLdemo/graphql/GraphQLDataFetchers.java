package com.nulpointerexception.cabdetailsgraphQLdemo.graphql;

import com.google.common.collect.ImmutableMap;
import com.nulpointerexception.cabdetailsgraphQLdemo.entities.Car;
import com.nulpointerexception.cabdetailsgraphQLdemo.entities.Driver;
import graphql.schema.DataFetcher;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class GraphQLDataFetchers {
//    private static final List<ImmutableMap<String, String>> cars = Arrays.asList(
//            ImmutableMap.of("id", "car1",
//                    "carNumber", "KA01H",
//                    "driverId", "12"),
//            ImmutableMap.of("id", "car2",
//                    "carNumber", "BR01H",
//                    "driverId", "13")
//    );
//
//    private static final List<Map<String, String>> drivers = new ArrayList(Arrays.asList(
//            ImmutableMap.of("id", "14",
//                    "firstName", "Harsh",
//                    "lastName", "Vardhan"),
//            ImmutableMap.of("id", "13",
//                    "firstName", "Rohit",
//                    "lastName", "Singh")
//    ));

    public DataFetcher<Car> getCabByIdDateFetcher(){
        return dataFetchingEnvironment -> {
            String carId = dataFetchingEnvironment.getArgument("id");
//            Map<String, String> carInStore =  cars.stream()
//                    .filter(car -> car.get("id").equals(carId))
//                    .findFirst()
//                    .orElse(null);
//            return carInStore;

            SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
            Session session = sessionFactory.openSession();
            Query<Car> q = session.createQuery("from Car where id = :carId");
            q.setParameter("carId",carId);
            System.out.println("Query fired from cars:" + q.getQueryString());
            List cars = q.list();
            session.close();
            return (Car) cars.get(0);
        };
    }

    public DataFetcher<Driver> getDriverDataFetcher(){
        return dataFetchingEnvironment -> {
            Car carInStore = dataFetchingEnvironment.getSource();
            System.out.println("driver entered is : " + carInStore.toString());
            Driver driver = carInStore.getDriverId();//carInStore.get("driverId");
//            return drivers.stream()
//                    .filter(driver -> driver.get("id").equals(driverId))
//                    .findFirst()
//                    .orElse(null);
//            SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
//            Session session = sessionFactory.openSession();
////            Query<DriverService> q = session.createQuery("from DriverService where id = :driverId");
////            q.setParameter("driverId",driverId);
////            DriverService driver = q.stream().findFirst().get();
//            session.close();
            return driver;
        };
    }

    public DataFetcher setDriverDataFetcher() {
        return dataFetchingEnvironment -> {
            String carId = dataFetchingEnvironment.getArgument("id");
            String firstName = dataFetchingEnvironment.getArgument("firstName");
            String lastName = dataFetchingEnvironment.getArgument("lastName");
            Map<String, String> driverDetails = new HashMap<>();
            driverDetails.put("firstName", firstName);
            driverDetails.put("lastName", lastName);
            driverDetails.put("id", carId);
//            drivers.add(driverDetails);
            return driverDetails;
        };
    }
}
