package com.nulpointerexception.cabdetailsgraphQLdemo.controller;

import com.nulpointerexception.cabdetailsgraphQLdemo.service.DriverService;
import com.nulpointerexception.cabdetailsgraphQLdemo.viewObjects.DriverListVO;
import com.nulpointerexception.cabdetailsgraphQLdemo.viewObjects.DriverVO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class DriverRestController {

    @RequestMapping(value = "/drivers")
    public ResponseEntity<DriverListVO> getAllDrivers()
    {
        DriverListVO driverListVO = DriverService.getAllDrivers();
        if(driverListVO.getDriverVOList().isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(driverListVO,HttpStatus.OK);
    }

    @RequestMapping(value = "/drivers/{id}")
    public ResponseEntity<DriverVO> getDriverById (@PathVariable("id") String id)
    {
        DriverVO driverVO = DriverService.getDriverById(id);
        if(driverVO == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(driverVO,HttpStatus.OK);
    }

}
