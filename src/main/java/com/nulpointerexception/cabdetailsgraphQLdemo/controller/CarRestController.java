package com.nulpointerexception.cabdetailsgraphQLdemo.controller;

import com.nulpointerexception.cabdetailsgraphQLdemo.service.CarService;
import com.nulpointerexception.cabdetailsgraphQLdemo.viewObjects.CarListVO;
import com.nulpointerexception.cabdetailsgraphQLdemo.viewObjects.CarVO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CarRestController {
    @RequestMapping(value = "/cars")
    public ResponseEntity<CarListVO> getAllCars()
    {
        CarListVO carListVO = CarService.getAllCars();
        if(carListVO.getCarVOList().isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(carListVO,HttpStatus.OK);
    }

    @RequestMapping(value = "/cars/{id}")
    public ResponseEntity<CarVO> getCarById (@PathVariable("id") String id)
    {
        CarVO carVO = CarService.getCarById(id);
        if(carVO == null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(carVO,HttpStatus.OK);
    }
}
