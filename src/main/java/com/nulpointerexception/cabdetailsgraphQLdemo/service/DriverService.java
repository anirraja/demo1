package com.nulpointerexception.cabdetailsgraphQLdemo.service;


import com.nulpointerexception.cabdetailsgraphQLdemo.controller.DriverRestController;
import com.nulpointerexception.cabdetailsgraphQLdemo.entities.Driver;
import com.nulpointerexception.cabdetailsgraphQLdemo.viewObjects.DriverListVO;
import com.nulpointerexception.cabdetailsgraphQLdemo.viewObjects.DriverVO;
import org.hibernate.QueryException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.hateoas.Link;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

public class DriverService {
    private static final Logger logger = LoggerFactory.getLogger(CarService.class);

    public static DriverListVO getAllDrivers(){
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
        Query<Driver> q = session.createQuery("from Driver");
        logger.debug("Query fired from drivers: {}" , q.getQueryString());
        List<Driver> drivers = q.list();
        session.close();
        if(drivers.isEmpty()){
            throw new QueryException("No drivers found in DB");
        }
        List<DriverVO> driverVOList = new ArrayList<>();
        for(Driver driver: drivers){
            Link driver_self_link =  linkTo(methodOn(DriverRestController.class).getDriverById(driver.getId())).withSelfRel();
            DriverVO driverVO = new DriverVO(driver);
            driverVO.add(driver_self_link);
            driverVOList.add(driverVO);
        }
        return new DriverListVO(driverVOList);
    }

    public static DriverVO getDriverById(String driverId) {
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
        Query<Driver> q = session.createQuery("from Driver where id = :driverId");
        q.setParameter("driverId", driverId);
        logger.debug("Query fired from drivers: {} with driverId {}", q.getQueryString(), driverId);
        List<Driver> drivers = q.list();
        session.close();
        if (drivers.isEmpty()) {
            throw new QueryException("No driver with id " + driverId + " found in DB");
        }
        Driver driver = drivers.get(0);
        DriverVO driverVO = new DriverVO(driver) ;
        Link driver_self_link =  linkTo(methodOn(DriverRestController.class).getDriverById(driver.getId())).withSelfRel();
        driverVO.add(driver_self_link);
        return driverVO;

    }
}
