package com.nulpointerexception.cabdetailsgraphQLdemo.service;

import com.nulpointerexception.cabdetailsgraphQLdemo.controller.CarRestController;
import com.nulpointerexception.cabdetailsgraphQLdemo.controller.DriverRestController;
import com.nulpointerexception.cabdetailsgraphQLdemo.entities.Car;
import com.nulpointerexception.cabdetailsgraphQLdemo.viewObjects.CarListVO;
import com.nulpointerexception.cabdetailsgraphQLdemo.viewObjects.CarVO;
import org.hibernate.QueryException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.hateoas.Link;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;


public class CarService {

    private static final Logger logger = LoggerFactory.getLogger(CarService.class);

    public static CarListVO getAllCars(){
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
        Query<Car> q = session.createQuery("from Car");
        logger.debug("Query fired from cars: {}" , q.getQueryString());
        List<Car> cars = q.list();
        session.close();
        if(cars.isEmpty()){
            throw new QueryException("No cars found in DB");
        }
        List<CarVO> carVOList = new ArrayList<>();
        for(Car car: cars){
            //add HATEOS links
            Link car_self_link = linkTo(methodOn(CarRestController.class).getCarById(car.getId())).withSelfRel();
            Link driver_self_link =  linkTo(methodOn(DriverRestController.class).getDriverById(car.getDriverId().getId())).withSelfRel();
            CarVO carVO = new CarVO(car);
            carVO.add(car_self_link);
            carVO.getDriverVO().add(driver_self_link);
            carVOList.add(carVO);

        }
        return new CarListVO(carVOList);
    }

    public static CarVO getCarById(String carId){
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
        Query<Car> q = session.createQuery("from Car where id = :carId");
        q.setParameter("carId",carId);
        logger.debug("Query fired from cars: {} with carId {}" , q.getQueryString(),carId);
        List<Car> cars = q.list();
        session.close();
        if(cars.isEmpty()){
            throw new QueryException("No car with id " + carId +" found in DB");
        }
        Car car = cars.get(0);
        CarVO carVO = new CarVO(car) ;
        //add HATEOS links
        Link car_self_link = linkTo(methodOn(CarRestController.class).getCarById(car.getId())).withSelfRel();
        Link driver_self_link =  linkTo(methodOn(DriverRestController.class).getDriverById(car.getDriverId().getId())).withSelfRel();
        carVO.add(car_self_link);
        carVO.getDriverVO().add(driver_self_link);
        return carVO;
    }
}
