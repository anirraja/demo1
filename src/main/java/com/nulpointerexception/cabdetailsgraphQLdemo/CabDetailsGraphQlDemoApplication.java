package com.nulpointerexception.cabdetailsgraphQLdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class CabDetailsGraphQlDemoApplication {


	public static void main(String[] args) {
		System.out.println("****************** GraphQL starts **************************");
		SpringApplication.run(CabDetailsGraphQlDemoApplication.class, args);
		System.out.println("****************** GraphQL ends **************************");
	}


}
