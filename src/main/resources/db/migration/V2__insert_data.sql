-- Table: public.drivers

INSERT INTO public.drivers(
	id, firstname, lastname)
	VALUES ('1', 'Harry', 'Potter');

INSERT INTO public.drivers(
	id, firstname, lastname)
	VALUES ('2', 'Ron', 'Weasley');

-- Table: public.cars

INSERT INTO public.cars(
	id, car_number, driver_id)
	VALUES ('1', 'KL01', '1');

INSERT INTO public.cars(
	id, car_number, driver_id)
	VALUES ('2', 'KL02', '2');

INSERT INTO public.cars(
	id, car_number, driver_id)
	VALUES ('3', 'KL03', '2');

INSERT INTO public.cars(
	id, car_number, driver_id)
	VALUES ('4', 'KL04', '1');
