package com.nulpointerexception.cabdetailsgraphQLdemo;

import java.util.List;

import com.nulpointerexception.cabdetailsgraphQLdemo.entities.Car;
import com.nulpointerexception.cabdetailsgraphQLdemo.entities.Driver;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.junit.Test;

public class HibernateConnectionTest {

    @Test

    public void crud() {
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

        Session session = sessionFactory.openSession();

        create(session);
        read(session);

        update(session);
        read(session);

        delete(session);
        read(session);

        session.close();
    }

    private void delete(Session session) {
        System.out.println("Deleting Mustang record...");
        Car mondeo = session.get(Car.class, "2");

        session.beginTransaction();
        session.delete(mondeo);
        session.getTransaction().commit();
    }

    private void update(Session session) {
        System.out.println("Updating Mustang driver...");
        Car mustang = session.get(Car.class, "2");
        Driver Harry = session.get(Driver.class, "1");
        mustang.setDriverId(Harry);

        session.beginTransaction();
        session.saveOrUpdate(mustang);
        session.getTransaction().commit();
    }

    private void create(Session session) {
        System.out.println("Creating new driver...");
        Driver Ron = new Driver();
        Ron.setId("2");
        Ron.setFirstname("Ron");
        Ron.setLastname("Weasley");

        session.beginTransaction();
        session.save(Ron);
        session.getTransaction().commit();

        System.out.println("Creating car records...");
        Car mustang = new Car();
        mustang.setId("2");
        mustang.setCarNumber("Mustang");
        mustang.setDriverId(Ron);

        session.beginTransaction();
        session.save(mustang);
        session.getTransaction().commit();
    }

    private void read(Session session) {
        Query<Car> q = session.createQuery("from Car");

        List<Car> cars = q.list();

        System.out.println("Reading car records...");
        System.out.printf("%-30.30s  %-30.30s%n", "Number", "DriverId");
        for (Car c : cars) {
            System.out.printf("%-30.30s  %-30.30s%n", c.getCarNumber(),c.getDriverId());
        }
    }
}